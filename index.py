import os
import requests
from googletrans import Translator
from dotenv import load_dotenv
from aiogram import Bot, Dispatcher, executor, types
import json
load_dotenv()
TELEGRAM_TOKEN = os.environ.get('TELEGRAM_TOKEN')
WEATHER_TOKEN = os.environ.get('WEATHER_TOKEN')

bot = Bot(token=TELEGRAM_TOKEN)
dp = Dispatcher(bot)
translator = Translator()


def from_kelvin_to_c(temp):
    return int(temp-273.15)


@dp.message_handler(commands=['start', 'help', 'cmds'])
async def start_message(message: types.Message):
    await message.reply('Привіт! Цей бот був створений для отримання інформації про погоди на даний момент!\
        Для того, аби взнати погоду, поділіться геолокацією з ботом ( команда /share_loc )')


def get_keyboard():
    keyboard = types.ReplyKeyboardMarkup()
    button = types.KeyboardButton("Share Position", request_location=True)
    keyboard.add(button)
    return keyboard


@dp.message_handler(content_types=['location'])
async def handle_location(message: types.Message):
    lat = message.location.latitude
    lon = message.location.longitude
    weather = requests.get(
        f'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={WEATHER_TOKEN}')
    if weather.status_code != 200:
        await message.answer('Спробуйте пізніше!', reply_markup=types.ReplyKeyboardRemove())
        print(weather.text)
        return
    info = weather.json()
    # print(info)
    result = f"Погода в {info['name']}, {info['sys']['country']}:\n\
        Наразі температура повітря {from_kelvin_to_c(info['main']['temp'])}°, відчувається як {from_kelvin_to_c(info['main']['feels_like'])}°.\n\
        Швидкість вітру: {info['wind']['speed']} м/c"

    await message.answer(result, reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(commands=['share_loc'])
async def get_location(message: types.Message):
    reply = "Натисніть, аби поділитися локацією з ботом!"
    await message.answer(reply, reply_markup=get_keyboard())

if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
